cards = [[:"1", :H], [:"1", :D], [:"1", :C], [:"1", :S], [:"2", :H], [:"2", :D], [:"2", :C], [:"2", :S], [:"3", :H], [:"3", :D], [:"3", :C], [:"3", :S], [:"4", :H], [:"4", :D], [:"4", :C], [:"4", :S], [:"5", :H], [:"5", :D], [:"5", :C], [:"5", :S], [:"6", :H], [:"6", :D], [:"6", :C], [:"6", :S], [:"7", :H], [:"7", :D], [:"7", :C], [:"7", :S], [:"8", :H], [:"8", :D], [:"8", :C], [:"8", :S], [:"9", :H], [:"9", :D], [:"9", :C], [:"9", :S], [:"10", :H], [:"10", :D], [:"10", :C], [:"10", :S], [:"11", :H], [:"11", :D], [:"11", :C], [:"11", :S], [:"12", :H], [:"12", :D], [:"12", :C], [:"12", :S], [:"13", :H], [:"13", :D], [:"13", :C], [:"13", :S]]

class PokerHandScorer
  def self.score(hand)
    suits(hand)
    suits_similarity
    ranks(hand)
  end

  #private
  # heaps of methods
  
  # case statement for which method returns true
  # https://www.cardplayer.com/rules-of-poker/hand-rankings

  def self.royal_flush?
    # A, K, Q, J, 10, all the same suit.
    # SUITS: all the same
    # RANKS: one each of 'A', 'K', 'Q', 'J', and '10'
    suits_similarity == 5 && @ranks_sequence && @ranks_list.include?(:"13")
  end

  def straight_flush?
    # Five cards in a sequence, all in the same suit.
    # SUITS: all the same
    # RANKS: all in sequence
    suits_similarity == 5 && ranks_sequence?
  end

  def self.four_of_a_kind?
    # All four cards of the same rank.
    # SUITS: any
    # RANKS: four the same
    @ranks_list[0..3].uniq.length == 1 || @ranks_list[1..4].uniq.length == 1
  end

  def self.full_house?
    # Three of a kind with a pair.
    three_of_a_kind? && pair?
  end

  def self.flush?
    # Any five cards of the same suit, but not in a sequence.
    # SUITS: all the same
    # RANKS: not a sequence of five
    suits_similarity == 5 && !ranks_sequence?
  end

  def straight?
    # Five cards in a sequence, but not of the same suit.
    # SUITS: not all the same
    # RANKS: sequence of five
    suits_similarity < 5 && ranks_sequence?
  end

  def three_of_a_kind?
    # Three cards of the same rank.
    # SUITS: any
    # RANKS: three the same, two different
    @ranks_list[0..2].uniq.length == 1 || @ranks_list[2..4].uniq.length == 1 #FIXME: remaining two can't be the same rank as each other
  end

  def two_pairs?
    # Two different pairs.
  end

  def pair?
    # Two cards of the same rank.
    # SUITS: 
    # RANKS: two the same, three different
  end

  def high_card?
    # When you haven't made any of the hands above, the highest card plays.
  end


  def self.ranks(hand)
    @ranks_list = []

    hand.each do |card|
      @ranks_list << card[0]
    end

    return @ranks_list.sort!
  end

  def self.ranks_sequence?(seq_num: 5)
    upper_limit = seq_num - 1
    @ranks_list[0..upper_limit]
      .map(&:to_s).map(&:to_i) # convert symbols to ints for comparison
      .each_cons(2).all? { |x,y| x == y - 1 } # checks all ranks are in a sequence
  end

  def self.suits(hand)
    @suits_list = {
      hearts: [],
      diamonds: [],
      clubs: [],
      spades: []
    }

    hand.each do |card| 
      case card[1]
      when :H 
        @suits_list[:hearts] << card[1]
      when :D
        @suits_list[:diamonds] << card[1]
      when :C 
        @suits_list[:clubs] << card[1]
      when :S 
        @suits_list[:spades] << card[1]
      end
    end

    return @suits_list
  end

  def self.suits_similarity
    #{:hearts=>[], :diamonds=>[], :clubs=>[], :spades=>[]}
    suits_count = [@suits_list[:hearts].length, @suits_list[:diamonds].length, @suits_list[:clubs].length, @suits_list[:spades].length]

    return suits_count.max
  end
end


straight_flush = [[:"8", :D], [:"7", :D], [:"6", :D], [:"5", :D], [:"4", :D]]
royal_flush = [[:"13", :D], [:"12", :D], [:"11", :D], [:"1", :D], [:"10", :D]]
four_of_a_kind = [[:"4", :D], [:"12", :D], [:"4", :H], [:"4", :C], [:"4", :S]]

game = PokerHandScorer
p game.score(straight_flush)
p game.four_of_a_kind?
p game.ranks_sequence?
