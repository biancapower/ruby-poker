class GameController
  def initialize
  end

  def play_game
  end
end

class HumanPlayerController
  def initialize(human_player)
    @human_player = human_player
  end

  def turn
  end
end

class PlayerView
  def self.show_hand(player)
  end

  def self.show_prompt(player)
  end
end

class PokerHandScorer
  def self.score(hand)
  end

  private
  # heaps of methods
end

class Deck < Array # is an array of cards
  def initialize
  end

  def shuffle
    super.shuffle
  end

  def take_from_top(number: 1)
  end
end

class Cards
  # create the cards
  # class methods only - no initialize
  # ['A','2','3','4','5','6','7','8','9','10','J','Q','K'] x ['♠','♣','♥','♦']

  @ranks = [:"1", :"2", :"3", :"4", :"5", :"6", :"7", :"8", :"9", :"10", :"11", :"12", :"13"]
  @suits = [:H, :D, :C, :S]

  def self.full_set
    cards = @ranks.product(@suits)
  end
end

p Cards.full_set

class Card
  attr_reader :number, :suit
  def initialize(number, suit)
    @number = number
    @suit = suit
  end
end

class Hand < Array
  # is a set of cards
end

class Player # abstract class - only inherit from
  # has a hand
  def initialize
    @hand = Hand.new
  end
end

class HumanIntelligence < Player
end

class ArtificialIntelligence < Player
end

=begin
class Holdings
end

# abstract class
class DealType
end
=end
